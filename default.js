$(document).ready(function() {
	parseData();

	fillData(1);

	$('#myForm input').on('change', function() {
		var dataType = $('input[name=radioName]:checked', '#myForm').val() ;
		fillData(dataType);
	});
});

/*
	dataType : 1: 初級會， 2: 男女青年
*/
var fillData = function(dataType) {
	var elmRoot = $('#result');

	elmRoot.html('');	//Clear Content

	//對於每一個單位
	$(aryUnits).each(function(index, unit) {
		var unitHeaderElement = makeUnitHeader(unit, elmRoot);
		//$(headerHTML).appendTo(elmRoot);
		//找出該單位中的每一個家庭
		var familiesInThisUnit = dicUnitFamilies[unit].families ;
		var html = "<table>";
		var memIndex = 0;
		$(familiesInThisUnit).each(function(indexMem, familyCode) {
			var familyMembers = dicFamilieMembers[familyCode];
			var famiResult = parseFamilyMembers(familyMembers);
			var head = famiResult.getHead() ;
			if (dataType == 2) {	//顯示男女青年
				var ymen = famiResult.getYoungMen();
				$(ymen).each(function(indexChild, child) {
					memIndex += 1;
					html += makeTRContent(child, head, memIndex);
				});
				var ywomen = famiResult.getYoungWomen();
				$(ywomen).each(function(indexChild, child) {
					memIndex += 1;
					html += makeTRContent(child, head, memIndex);
				});
			}
			else { 	//預設是初級會
				var children = famiResult.getChildren();
				if (children.length > 0) {
					console.log(children);
				}
				$(children).each(function(indexChild, child) {
					memIndex += 1;
					html += makeTRContent(child, head, memIndex);
				});
			}
		});
		html += "</table>";
		var targetTable = $(html).appendTo(elmRoot);

		attachDownloadEvent(unitHeaderElement, html);
	});

}

var attachDownloadEvent = function(unitHeaderElement, table_html) {
	var resultHTML = "<!doctype html><html><head><meta charset='utf-8'><title>" + unitHeaderElement.text() + "</title></head><body>" + table_html + "</body></html>";
	
	$("<button style='display:inline-block;float:right'>Download</button>")
		.appendTo(unitHeaderElement)
		.click(function() {
			//getting values of current time for generating the file name
	        var a = document.createElement('a');
	        //getting data from our div that contains the HTML table
	        var data_type = 'data:application/vnd.ms-excel';
	        // var table_div = document.getElementById('dvData');
	        // var table_html = table_div.outerHTML.replace(/ /g, '%20');
	        a.href = data_type + ', ' + table_html;
	        //setting the file name
	        a.download = unitHeaderElement.text() + '.xls';
	        //triggering the function
	        a.click();
	        //just in case, prevent default behaviour
	        e.preventDefault();

			// window.open('data:application/vnd.ms-excel,' + encodeURIComponent(resultHTML));
	});
};

var makeUnitHeader = function(unit, elmRoot) {
	var divUnitHeader = $('<div class="unit">').appendTo(elmRoot);
	$("<span>" + unit + "</span>").appendTo(divUnitHeader);
	return divUnitHeader ;
}

var makeTRContent = function(child, head, indexChild) {
	var html = '<tr>';
	html += ('<td>' + (indexChild) + "</td>");
	html += ('<td>' + child.name + " ( " + child.age + ", " + child.genderLabelShort + " )</td>");
	html += ('<td>' + head.name + " ( " + head.age +  ", " + head.genderLabelShort + " )</td>");
	html += ('<td>' + head.priesthood + "</td>");
	html += ('<td>' + head.birthDate + "</td>");
	html += ('<td>' + head.phone + "</td>");
	html += ('<td>' + head.householdPhone + "</td>");
	html += ('<td>' + purifyAddress(head.address) + "</td>");
	html += ('</tr>');
	return html ;
}

// Families
var dicFamilieMembers = {};
var aryFamilies = [];

var dicMembers = {};

// Units
var dicUnitFamilies = {};
var aryUnits = [];

var parseData = function() {
	console.log("member count : " + members.length );
	$(members).each(function(index, mem) {
		var familyID = mem.householdId ;
		if (!dicFamilieMembers[familyID]) {
			dicFamilieMembers[familyID] = [];
			aryFamilies.push(familyID);
		}
		dicFamilieMembers[familyID].push(mem);

		var unitName = mem.unitName;
		if (!dicUnitFamilies[unitName]) {
			dicUnitFamilies[unitName] = { "families" : [], dicFamily : {}};
			aryUnits.push(unitName);
		}
		if (!dicUnitFamilies[unitName].dicFamily[familyID]) {
			dicUnitFamilies[unitName].dicFamily[familyID] = true ;
			dicUnitFamilies[unitName].families.push(familyID);
		}
	});
};

var purifyAddress = function(address) {
	return address.replace(/<br \/>/g, "  ");
}

/*
	解析家庭成員中，誰是家長，誰是男青年，誰是女青年，誰是初級會小朋友
*/
var parseFamilyMembers = function(mems) {
	var _head = null ;
	var _men = [];
	var _women = [];
	var _ymen = [];
	var _ywomen = [];
	var _childern = [];

	$(mems).each(function(index, mem) {
		if (mem.age < 12) {
			_childern.push(mem);
		}
		else if (mem.age < 18) {
			if (mem.gender == "MALE") {
				_ymen.push(mem);
			}
			else {
				_ywomen.push(mem);
			}
		}
		else {
			if (mem.gender == "MALE") {
				_men.push(mem);
			}
			else {
				_women.push(mem);
			}
		}

		if (mem.isHead) {
			_head = mem ;
		}
	});

	return  {
		getHead : function() {
			return _head;
		},

		getMen : function() {
			return _men ;
		},

		getWomen : function() {
			return _women ;
		},

		getYoungMen : function() {
			return _ymen ;
		},

		getYoungWomen : function() {
			return _ywomen ;
		},

		getChildren : function() {
			return _childern ;
		},


	}

}